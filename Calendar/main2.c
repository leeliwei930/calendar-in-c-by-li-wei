#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


int year, yearDays;
//declare an array set all every month's days , leap year will be change the array value by using the if 
int daysInMonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
char *monthName[] = { "" , "\n\n\n January" , "\n\n\n February" ,  "\n\n\n March" ,"\n\n\n April" , "\n\n\n May" , "\n\n\n June" ,
"\n\n\n July" , "\n\n\n August" , "\n\n\n September", "\n\n\n October" ,  "\n\n\n November", "\n\n\n December" };

int main() {
	//Standard Prompt Out Message, and input the year data
	printf("Calendar In C Version 2.0\n");
	printf("Enter The Year You Want To See:");
	scanf("%d", &year);

	/* leap Year Detection by using remainder, if the year divide by 4 no remain mean the february have 29 days (Totally 366 days a year)
	else 28 days (365 days a year) */
	if (year % 4 == 0 && year % 100 != 0 || year % 400 != 0) {
		daysInMonth[2] = 29;
		yearDays = 366;
		//change the daysInMonth 2 array to 29
	}
	else {
		daysInMonth[2] = 28;
		//default value
		yearDays = 365;
	}
	int dayCode;


	dayCode = (year + ((year - 1) / 4) -( (year - 1) / 100) + ( (year - 1) / 400)) % 7;
	printf("\t \t \t Year: %d \n", year);

	for (int month = 1 ; month <= 12; month++) {
		printf("\t \t \t %s \n", monthName[month]);
		printf("Sun  Mon  Tue  Wed  Thu  Fri  Sat\n");
		for (int beginDay = 1; beginDay <= (1 + dayCode * 5); beginDay++) {
			printf(" ");




		}
		for (int day = 1; day <= daysInMonth[month]; day++)
		{
			printf("%2d", day);

			if ((day + dayCode) % 7 > 0)
				printf("   ");
			else
				printf("\n ");
		}

		//Carried On Next Month
		dayCode = (dayCode + daysInMonth[month]) % 7;

	}
	printf("\n");

	getch();
	return 0;
}